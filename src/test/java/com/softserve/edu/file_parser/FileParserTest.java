package com.softserve.edu.file_parser;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by alin- on 15.12.2017.
 */
public class FileParserTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private File file;
    private Writer writer;

    @Before
    public void createTestingFile() throws IOException {
        file = folder.newFile();
        writer = new FileWriter(file);
    }

    @Test
    public void countStringsInFileTestStringHello() throws IOException {
        writer.write("hello hello ldmcklwec\n" +
                "wcemcmwlkcmhello\n" +
                "HELLO\n" +
                "cekncke\n" +
                "helloHELLOHELLO");
        writer.close();
        assertEquals(7, FileParser.countStringInFile(file.getPath(), "hello"));
    }

    @Test
    public void countStringsInFileTestStringBoy() throws IOException {
        writer.write("boyboyBOYBOYBOY\n" +
                "wcemcmwlkcmhello\n" +
                "BOYBOY\n" +
                "boyboy\n" +
                "BOY");
        writer.close();
        assertEquals(10, FileParser.countStringInFile(file.getPath(), "boy"));
    }

    @Test
    public void countStringsInFileTestStringHouse() throws IOException {
        writer.write("wekfnkewfnewfkfn" +
                "efknekfnefnNLKFNKFDF" +
                "LLSDLSDLSD" +
                "SLDMLSDMShouse");
        writer.close();
        assertEquals(1, FileParser.countStringInFile(file.getPath(), "house"));
    }

    @Test
    public void replaceStringInFileTestCatToDog() throws IOException {
        writer.write("wfkefnkenkcat" +
                "cat my little cat" +
                "skskkkksk dog cat" +
                "favourite cat");
        writer.close();
        assertEquals(5, FileParser.replaceStringInFile(file.getPath(), "cat", "dog"));
    }

    @Test
    public void replaceStringInFileTest12To19() throws IOException {
        writer.write("122929929229925463384" +
                "0987654312;dw" +
                "fewjnfjkewfnefw12dnfke" +
                "sf's'df,'sfc,;sd,sd;lffd;s12");
        writer.close();
        assertEquals(4, FileParser.replaceStringInFile(file.getPath(), "12", "19"));
    }

    @Test
    public void replaceStringInFileTestFiftyTo50() throws IOException {
        writer.write("122929929229925463384" +
                "fifty students" +
                "fifty children" +
                "sf's'df,'sfc,;sd,sd;lffd;s12");
        writer.close();
        assertEquals(2, FileParser.replaceStringInFile(file.getPath(), "fifty", "50"));
    }
}