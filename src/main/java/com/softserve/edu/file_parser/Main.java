package com.softserve.edu.file_parser;

/**
 * Created by alin- on 10.12.2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length == 2) {
            System.out.println("String \"" + args[1] + "\" appears " +
                    FileParser.countStringInFile(args[0], args[1]) + " times");
        } else if (args.length >= 3){
            System.out.println(FileParser.replaceStringInFile(args[0], args[1], args[2]));
        }
        else System.out.println("You must input at least 2 params: filePath and string!");
    }
}



