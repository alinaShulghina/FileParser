package com.softserve.edu.file_parser;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alin- on 10.12.2017.
 */
public class FileParser {
    /*
        Count amount of given string in file
     */
    static int countStringInFile(String filePath, String stringToCount) {
        int count = 0;
        String fileText = readTextFromFile(filePath);
        Pattern pattern = Pattern.compile(stringToCount, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(fileText);
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    /*
        Replace given string with a new string in file
        Return number of replaced Strings
     */
    static int replaceStringInFile(String filePath, String oldString, String newString) {
        int totalReplacedStrings = 0;
        String fileText = readTextFromFile(filePath);
        Pattern pattern = Pattern.compile(oldString, Pattern.LITERAL);
        Matcher matcher = pattern.matcher(fileText);
        StringBuffer stringBuffer = new StringBuffer();
        while (matcher.find()) {
            totalReplacedStrings++;
            matcher.appendReplacement(stringBuffer, newString);
        }
        matcher.appendTail(stringBuffer);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(stringBuffer.toString());
        } catch (IOException e) {
            System.out.println("Unable to write data to file!");
        }
        return totalReplacedStrings;
    }

    /*
        Read text from given file
     */
    public static String readTextFromFile(String filePath) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find file! Please check the path!");
        } catch (IOException e) {
            System.out.println("Error reading the file!");
        }
        return stringBuilder.toString();
    }
}
